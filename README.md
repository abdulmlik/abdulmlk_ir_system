# abdulmlk_IR_system
Sample  Information Retrieval System using WPF application in C#

ind

*****  meaning end page

How to compile
==============

1. Install software

- Git <br />
  https://git-for-windows.github.io/ <br />
  Select a file summarized as "Full installer for official Git for Windows" <br />
   with the highest version
- Visual Studio 
  http://www.visualstudio.com/downloads/download-visual-studio-vs <br />
  Select "Visual Studio Community for Windows Desktop" version 15.3 or newer (to include .NET standard 2.0).
- Microsoft .NET 4.0
- .NET standard 2.0

2. Check out

- Create an empty folder anywhere
- In explorer right click and select "Git Bash" then git clone URL  <br />
  set URL https://github.com/abdulmlik/abdulmlk_IR_system.git <br />
  enter

3. Build

- Open IR_SystemProjects.sln with Visual Studio for windows desktop.
- Compile.

=============


This project is a homework, <br />
I do not allow using it as homework for any student <br />
Otherwise allow it to be used, <br />
So do not commit academic dishonesty.
